(* ::Package:: *)

(* :Title: Micro CT Tools *)

(* :Context: MicroCtTools` *)

(* :Author:
	Matt Latourette
*)

(* :Copyright: Copyright 2015, Michigan State University *)

(* :Package Version: 1.0 *)

(* :Mathematica Version: 10.0 *)

(* :Summary:
This package implements functions that facilitate import, visualization, and manipulation of data generated 
by micro computed tomography. Import functionality is designed to support data obtained in VFF format from 
a Locus RS scanner and converted using a companion routine called saveVffForMathematica which is run from 
MATLAB to convert the VFF files to MATLAB version 5 files.
*)

(* :Keywords: micro CT, computed tomography, image processing
*)

(* :History:
	Original Version created by Matt Latourette, 2015.
*)

(* :Requirements: No special system requirements. *)

(* :Warnings: None. *)

(* :Limitations: None identified at this time. *)

(* :Sources:
Wolfram Research, Create a 3D Image Slicer: Wolfram Language Featured Example.
*)

(* :Discussion:
*)

BeginPackage["MicroCtTools`"];

Unprotect[ImportMicroCtImage3D, ImportMicroCtImage3DFromDicom, Image3DInteractiveSlicer, RegionOfInterest, InteractiveViewType, OverlayColor, 
OverlayOpacity, StackView, MultiplanarView, SegmentationVolume, VoxelDimensions, AirHounsfieldUnits, 
RegionBlank, BlankSelection, Inside, Outside, BlankValue, GetImageMetaInformation, Image3DComparison, 
FindThresholdLocalMinimumMethod, FindThresholdPerpendicularDistanceMethod, 
FindThresholdSecondDerivativeMaximumMethod, FindThresholdFromHistogram, PlotSmoothAndRoughHistograms, 
Image3DInteractiveContourPlot, MaskUnion, MaskIntersection];

ImportMicroCtImage3D::usage = "ImportMicroCtData[filename] reads micro CT data that has first been 
converted from the original VFF format to a MATLAB file using the saveVffForMathematica function (in 
MATLAB), extracts the 3D image and metadata from the MATLAB struct, and returns a list of key-value 
pairs in the form of a Mathematica rule list.";

ImportMicroCtImage3DFromDicom::usage = "ImportMicroCtData[path, filenameroot, digits] reads micro CT data in 
DICOM format from the filesystem path specified and extracts a subset of the DICOM header information into a 
list of key-value pairs in the forma of a Mathematica rule list. It is assumed that a set of DICOM files 
constituting a 3D stack is located in the specified path and that the files are ordered numerically with their
filenames as specified by filenameroot, followed by a zero-padded integer having the specified number of digits,
with a trailing .dcm filename suffix. Isotropic voxels are assumed with voxel dimensions given by the 
SliceThickness in the DICOM metadata.";

Image3DInteractiveSlicer::usage = "Image3DInteractiveSlicer[image] shows slices through the 
specified 3D image using an interactive interface that allows the user to manipulate slider(s) to change 
the cut plane(s) shown. A color overlay depicting a region of interest may optionally be shown on the 
image slices. Either an image stack or 3-plane view of the image may be shown.";
Options[Image3DInteractiveSlicer]={InteractiveViewType->StackView, RegionOfInterest->None, OverlayColor->Blue, 
OverlayOpacity->0.3};

RegionOfInterest::usage = "An option specifying a binary image where voxels having nonzero values are 
defined to be within the region of interest and voxels having a value of zero are defined to be outside 
of the region.";

InteractiveViewType::usage = "An option specifying the kind of display to be shown by the 
Image3DInteractiveSlicer interface. Valid values are StackView and MultiplanarView.";

OverlayColor::usage = "An option specifying the color to be used in diplaying a RegionOfInterest as an 
overlay in the Image3DInteractiveSlicer interface.";

OverlayOpacity::usage = "An option specifying the opacity of the color overlay used to display a 
RegionOfInterest in the Image3DInteractiveSlicer interface.";

StackView::usage = "An option value specifying that a stack of slices representing cut planes through the 
first dimension of the Image3D volume are to be displayed in the Image3DInteractiveSlicer interface.";

MultiplanarView::usage = "An option value specifying that three stacks of slices representing cut planes 
through each dimension of the Image3D volume are to be displayed in the Image3DInteractiveSlicer interface.";

SegmentationVolume::usage = "SegmentationVolume[binaryImage] computes the volume of the region of 
binaryImage with nonzero voxel values.";

VoxelDimensions::usage = "VoxelDimensions[image] extracts information about the size of the 3D image 
voxels from the metadata. The result is a 3D vector represented as a list.";

AirHounsfieldUnits::usage = "A constant specifying the value for air in the Hounsfield Unit scale.";

RegionBlank::usage = "RegionBlank[sourceImage, binaryMaskImage] returns a modified version of sourceImage 
in which some voxel values have been set to the value specified by the BlankValue option. The 
BlankSelection option determines whether to change the values of the voxels Inside or Outside of the 
binaryMaskImage. By default, BlankValue is set to the value of the AirHounsfieldUnits constant (-1000) and 
the BlankSelection is set to Outside.";
Options[RegionBlank] = {BlankValue->AirHounsfieldUnits, BlankSelection->Outside};

BlankSelection::usage = "An option for the RegionBlank function which specifies if blanking is to occur 
Inside or Outside the specified region of interest.";

Inside::usage = "An option value that specifies blanking occurs where the voxel values of the region mask 
are nonzero.";

Outside::usage = "An option value that specifies blanking occurs where the voxel values of the region mask 
are zero.";

BlankValue::usage = "An option value that specifies the value to use as a replacement when blanking voxels 
in a region using the RegionBlank function.";

GetImageMetaInformation::usage = "";

Image3DComparison::usage = "Image3DComparison[image1, image2] displays the 3D images side by side and 
synchronizes their viewpoints and vertical vectors to enable easy comparison of segmentations derived from
the same source image.";
Options[Image3DComparison] = {SphericalRegion->True};

FindThresholdLocalMinimumMethod::usage = "FindThresholdLocalMinimumMethod[image3d, lowerLimit, upperLimit] 
automatically computes a threshold at the minimimum of the image's smoothed histogram on the interval between 
lowerLimit and upperLimit.";

FindThresholdPerpendicularDistanceMethod::usage = "FindThresholdPerpendicularDistanceMethod[image3d, lowerLimit, upperLimit] automatically
computes a threshold at the point on the image's smoothed histogram PDF in the interval between lowerLimit and
upperLimit where the greatest perpendicular distance exists between that point and the line passing through the 
global maximum of the smoothed histogram and the value of the smoothed histogram at the specified upperLimit.";
Options[FindThresholdPerpendicularDistanceMethod]={UpperAnchorPoint->600};

FindThresholdSecondDerivativeMaximumMethod::usage = "FindThresholdSecondDerivativeMaximumMethod[image3d, 
lowerLimit, upperLimit] automatically computes a threshold at the point within the interval from lowerLimit to 
upperLimit where the second derivative of the smoothed histogram is maximal.";

FindThresholdFromHistogram::usage = "Automatically computes a threshold for segmenting a region containing
high-Z contrast material from ordinary soft tissue.";

PlotSmoothAndRoughHistograms::usage = "PlotSmoothAndRoughHistograms[image3d] computes a smooth approximating 
histogram and displays it overlaid on top of a standard histogram with bin size set by the RoughHistogramBinSize
option.";
(*Options[PlotSmoothAndRoughHistograms]= {RoughHistogramBinSize->10, PlotRange->All, ImageSize->Large, 
BaseStyle->{FontSize->18}};*)
Options[PlotSmoothAndRoughHistograms]={PlotRange->Full, ImageSize->Large, AxesOrigin->{0,0}};

Image3DInteractiveContourPlot::usage = "Image3DInteractiveContourPlot[image3d] shows contour plots of 
individual slices through the specified 3D image using an interactive interface that allows the user to 
manipulate a slider to change the slice shown.";
Options[Image3DInteractiveContourPlot]={ImageSize->Large, Contours->10};

MaskUnion::usage = "MaskUnion[mask1, mask2] computes the union of two binary image masks. For each pixel
(voxel), the resulting value is 1 if either of the corresponding pixels (voxels) in the masks is 1.";

MaskIntersection::usage = "MaskIntersection[mask1, mask2] computes the intersection of two binary image 
masks. For each pixel (voxel), the resulting value is 1 if both of the corresponding pixels (voxels) in the
masks is 1, and 0 otherwise.";


Begin["`Private`"];

(*============================ package dependencies (Needs) =====================================*)

(*==================================== constants ==============================================*)

AirHounsfieldUnits = -1000.0;

(*==============================\[Equal] function definitions =========================================*)


MaskIntersection[image1_, image2_]:=ImageApply[maskIntersectionCompiledFunction, {image1, image2}];

MaskUnion[image1_, image2_]:=ImageApply[maskUnionCompiledFunction, {image1, image2}];


maskIntersectionCompiledFunction = 
	Compile[{{pixel1, _Real}, {pixel2, _Real}},
		If[pixel1 > 0.0 && pixel2 > 0.0,
			1,
			0
		]
	];

maskUnionCompiledFunction = 
	Compile[{{pixel1, _Real}, {pixel2, _Real}}, 
		If[pixel1 > 0.0, 
			1, 
			If[pixel2 > 0.0, 1, 0]
		]
	];


PlotSmoothAndRoughHistograms[image3d_Image3D, opts : OptionsPattern[{PlotSmoothAndRoughHistograms, Plot}]]:=
	With[{data = Flatten@ImageData[image3d], binSize=1},
		With[{lowerBinLimit=Ceiling[Min[data]-binSize, binSize]+1, 
			upperBinLimit=Floor[Max[data]+binSize, binSize]-1},
			With[{xvals=Range[lowerBinLimit, upperBinLimit, binSize],
				yvals = BinCounts[data, {lowerBinLimit, upperBinLimit, binSize}]},
				With[{filteredYVals = N[MeanFilter[yvals, 30]]},
					With[{filteredHistogram = Partition[Riffle[xvals, filteredYVals],2],
						unfilteredHistogram = Partition[Riffle[xvals, yvals], 2]},
						With[{smoothHistogram = Interpolation[filteredHistogram],
							roughHistogram = Interpolation[unfilteredHistogram]},
							With[{plotOpts = FilterRules[Options[PlotSmoothAndRoughHistograms], 
								Options[Plot]]},
								With[{smoothplot = Plot[smoothHistogram[x], {x, lowerBinLimit, upperBinLimit},
										opts, Sequence[plotOpts], PlotStyle->{Black, AbsoluteThickness[1]}],
									roughplot = Plot[roughHistogram[x], {x, lowerBinLimit, upperBinLimit},
										opts, Sequence[plotOpts], PlotStyle->{Red, Opacity[0.5], AbsoluteThickness[0.5]}]},
									Show[{smoothplot, roughplot}]]]]]]]]];


FindThresholdLocalMinimumMethod[image3d_Image3D, upperLimit_?NumericQ, OptionsPattern[]]:=
	With[{data = Flatten@ImageData[image3d], binSize=1},
		With[{lowerBinLimit=Ceiling[Min[data]-binSize, binSize]+1,
			upperBinLimit=Floor[Max[data]+binSize, binSize]-1},
			With[{xvals=Range[lowerBinLimit, upperBinLimit, binSize], 
				yvals=N[MeanFilter[BinCounts[data, {lowerBinLimit, upperBinLimit, binSize}], 100]]},
				With[{filteredHistogram = Partition[Riffle[xvals, yvals],2]},
					With[{smoothHistogram = Interpolation[filteredHistogram]},
						With[{histmax=NMaximize[{smoothHistogram[x], lowerBinLimit+binSize<=x<=upperBinLimit-binSize}, x]},
							With[{constrainedLowerLimit = Max[Replace[x, histmax[[2]]], 0],
							constrainedUpperLimit = Min[upperLimit, upperBinLimit-binSize]},
								First@Union[Table[Replace[x, NMinimize[{smoothHistogram[x], 
								lowerBound<=x<=constrainedUpperLimit}, x][[2,1]]], 
								{lowerBound, constrainedLowerLimit, constrainedUpperLimit, 50*binSize}], 
								SameTest->Function[{u,v}, If[u-v<1, True, False]]]
							]]]]]]];

FindThresholdPerpendicularDistanceMethod[image3d_Image3D, upperLimit_?NumericQ, 
	OptionsPattern[]]:=With[{data = Flatten@ImageData[image3d], binSize=1},
	With[{lowerBinLimit=Ceiling[Min[data]-binSize, binSize]+1,
		upperBinLimit=Floor[Max[data]+binSize, binSize]-1},
		With[{xvals=Range[lowerBinLimit, upperBinLimit, binSize], 
			yvals=N[MeanFilter[BinCounts[data, {lowerBinLimit, upperBinLimit, binSize}], 30]]},
			With[{filteredHistogram = Partition[Riffle[xvals, yvals],2]},
				With[{smoothHistogram = Interpolation[filteredHistogram]},
					With[{histmax=NMaximize[{smoothHistogram[x], lowerBinLimit+binSize<=x<=upperBinLimit-binSize}, x]},
						With[{constrainedLowerLimit = Max[Replace[x, histmax[[2]]], 0],
						constrainedUpperLimit = Min[upperLimit, upperBinLimit-binSize]},
							With[{a=Replace[x, histmax[[2]]], b=histmax[[1]], c=Min[OptionValue[UpperAnchorPoint], constrainedUpperLimit]},
								With[{d=smoothHistogram[c]},
									Replace[x,
										NMaximize[
											{Norm[
												{a-x, b-smoothHistogram[x]} - 
												Projection[{a-x, b-smoothHistogram[x]}, {a-c, b-d}]], 
												constrainedLowerLimit<=x<=constrainedUpperLimit
											}
										,x][[2]]]
								]]]]]]]]];

FindThresholdSecondDerivativeMaximumMethod[image3d_Image3D, upperLimit_?NumericQ, OptionsPattern[]]:=
	With[{data = Flatten@ImageData[image3d],binSize=1},
		With[{lowerBinLimit=Ceiling[Min[data]-binSize, binSize]+1,upperBinLimit=Floor[Max[data]+binSize, binSize]-1},
			With[{xvals=Range[lowerBinLimit, upperBinLimit, binSize],yvals = N[MeanFilter[
				BinCounts[data, {lowerBinLimit, upperBinLimit, binSize}],30]]},
				With[{histdata=Partition[Riffle[xvals, yvals], 2]},
					With[{firstDerivativeXVals=xvals+binSize/2, firstDerivativeYVals=N[MeanFilter[Differences[yvals],30]]},
						With[{secondDerivativeXVals=firstDerivativeXVals+binSize/2, secondDerivativeYVals=N[MeanFilter[
							Differences[firstDerivativeYVals],40]]},
							With[{secondDerivative=Partition[Riffle[secondDerivativeXVals, secondDerivativeYVals],2]},
								With[{histSecondDerivative=Interpolation[secondDerivative]},
									With[{filteredHistogram = Partition[Riffle[xvals, yvals],2]},
										With[{smoothHistogram = Interpolation[filteredHistogram]},
											With[{histmax=NMaximize[{smoothHistogram[x], lowerBinLimit+binSize<=x<=upperBinLimit-binSize}, x]},
												With[{constrainedLowerLimit = Max[Replace[x, histmax[[2]]], 0],
												constrainedUpperLimit = Min[upperLimit, upperBinLimit-binSize]},
													Replace[x,
													NMaximize[{histSecondDerivative[x], constrainedLowerLimit<=x<=constrainedUpperLimit}, 
														x
													][[2]]]
											]]]]]]]]]]]];

FindThresholdFromHistogram[image3d_Image3D, upperLimit_?NumericQ, 
	opts : OptionsPattern[{FindThresholdFromHistogram, FindThresholdLocalMinimumMethod, 
		FindThresholdPerpendicularDistanceMethod, FindThresholdSecondDerivativeMaximumMethod}]]:=
	With[{data = Flatten@ImageData[image3d], binSize=1},
		With[{lowerBinLimit=Ceiling[Min[data]-binSize, binSize]+1, 
			upperBinLimit=Floor[Max[data]+binSize, binSize]-1},
			With[{xvals=Range[lowerBinLimit, upperBinLimit, binSize], 
				yvals=N[MeanFilter[BinCounts[data, {lowerBinLimit, upperBinLimit, binSize}], 30]]},
				With[{filteredHistogram = Partition[Riffle[xvals, yvals],2]},
					With[{smoothHistogram = Interpolation[filteredHistogram]},
						With[{histmax=NMaximize[{smoothHistogram[x], lowerBinLimit+binSize<=x<=upperBinLimit-binSize}, x]},
							With[{waterPeakX = Replace[x, histmax[[2]]]},
								With[{foundMinimum = FindThresholdLocalMinimumMethod[image3d,  
									upperLimit, Sequence @@ FilterRules[{opts}, 
									Options[FindThresholdLocalMinimumMethod]]]},
									With[{maxAbove = Replace[x,NMaximize[{smoothHistogram[x], 
										foundMinimum<=x<=upperBinLimit-binSize}, x][[2,1]]]},
										If[smoothHistogram[maxAbove]-smoothHistogram[foundMinimum]>1,
											foundMinimum, 
											FindThresholdSecondDerivativeMaximumMethod[image3d,  
												upperLimit, opts, Sequence @@ FilterRules[Options[FindThresholdFromHistogram], 
												Options[FindThresholdSecondDerivativeMaximumMethod]]]]]]]]]]]]];



RegionBlank[sourceImage_, binaryMaskImage_, OptionsPattern[]]:=
	Module[{selection = OptionValue[BlankSelection], blank = OptionValue[BlankValue]},
		Switch[
			selection,
			Inside,
				ImageAdd[
					ImageMultiply[sourceImage, ColorNegate[binaryMaskImage]], 
					ImageMultiply[binaryMaskImage, blank]
				],
			Outside,
				ImageAdd[
					ImageMultiply[sourceImage, binaryMaskImage],
					ImageMultiply[ColorNegate[binaryMaskImage], blank]
				]
		]
	];


VoxelDimensions[image3d_Image3D]:=
	Module[{metadata = GetImageMetaInformation[image3d], voxdims, elemsize},
		If[KeyExistsQ[metadata, "ElementSize"],
			elemsize = Replace["ElementSize", metadata];
			If[KeyExistsQ[metadata, "Spacing"],
				elemsize*Replace["Spacing", metadata],
				elemsize*{1.0, 1.0, 1.0}
			],
			If[KeyExistsQ[metadata, "Spacing"],
				Replace["Spacing", metadata],
				$Failed
			]
		]
	];


(* Note: a binary image is assumed *)
SegmentationVolume[image3d_Image3D]:=
	Module[{voxcount = Total@Flatten@ImageData[image3d], voxdims = VoxelDimensions[image3d]},
		voxcount*voxdims[[1]]*voxdims[[2]]*voxdims[[3]]
	];


GetImageMetaInformation[image_]:=Replace[MetaInformation, Options[image, MetaInformation]];


ImportMicroCtImage3D[filename_]:= 
	Module[{raw, dataStruct, ruleList, metadata, vol}, 
		raw = Import[filename, "LabeledData"];
		dataStruct = Replace["vol", raw];
		metadata = MicroCtDataStructToRuleList[dataStruct];
		vol = If[MemberQ[Keys@dataStruct, "ImageStack"], Replace["ImageStack", dataStruct]];
		Image3D[vol, "Real", ColorSpace->"Grayscale", ColorFunction->"XRay", Background->Black, 
			ImageSize->Medium, MetaInformation->metadata]
	];
	
(* ImportMicroCtImage3DFromDicom function added 2/18/2020 by Matt Latourette, adds support for images in DICOM format to enable
use of images obtained from the new PerkinElmer Quantum GX micro CT *)
ImportMicroCtImage3DFromDicom[path_, filenameroot_, digits_]:=
	With[{dicomimageheader = Import[path<>"\\"<>filenameroot<>IntegerString[1,10,digits]<>".dcm","Rules"]},
		Module[{
			numberofframes="(0031,1001)"/. ("MetaInformation"/.dicomimageheader),
			rescaleslope= "RescaleSlope"/.("MetaInformation"/.dicomimageheader),
			rescaleintercept="RescaleIntercept"/.("MetaInformation"/.dicomimageheader)
			},
		Image3D[
			Map[
				Import[
					FileNameJoin[{path,filenameroot<>IntegerString[#1,10,digits]<>".dcm"}],
					"Data"][[1,;;,;;]]*rescaleslope+rescaleintercept &,
				Range[1,numberofframes,1]],
			Background->GrayLevel[0], ColorFunction->"XRay", ColorSpace->"Grayscale", ImageSize->Medium, Interleaving->None, 
			MetaInformation-><|"Path"->path, "Filename"->filenameroot<>IntegerString[1,10,digits]<>".dcm", 
				"Rows"->("Rows" /. ("MetaInformation"/.dicomimageheader)), 
				"Columns"->("Columns"/.("MetaInformation"/.dicomimageheader)), 
				"Frames"->("(0031,1001)"/.("MetaInformation"/.dicomimageheader)), 
				"Bits"->("BitDepth"/.dicomimageheader), 
				"Spacing"->{1.`,1.`,1.`}, 
				"ElementSize"->("SliceThickness"/.("MetaInformation"/.dicomimageheader))|>
	]]];


MicroCtDataStructToRuleList[ds_]:= 
	Module[{l = List[], k = Keys@ds}, 
		If[MemberQ[k, "Path"], AppendTo[l, "Path"->FromCharacterCode[Replace["Path", ds]]]];
		If[MemberQ[k, "Filename"], AppendTo[l, "Filename"->FromCharacterCode[Replace["Filename", ds]]]];
		If[MemberQ[k, "Rows"], AppendTo[l, "Rows"->First@First@Replace["Rows", ds]]];
		If[MemberQ[k, "Columns"], AppendTo[l, "Columns"->First@First@Replace["Columns", ds]]];
		If[MemberQ[k, "Frames"], AppendTo[l, "Frames"->First@First@Replace["Frames", ds]]];
		If[MemberQ[k, "Bits"], AppendTo[l, "Bits"->First@First@Replace["Bits", ds]]];
		If[MemberQ[k, "Rank"], AppendTo[l, "Rank"->First@First@Replace["Rank", ds]]];
		If[MemberQ[k, "Type"], AppendTo[l, "Type"->FromCharacterCode[Replace["Type", ds]]]];
		If[MemberQ[k, "Origin"], AppendTo[l,"Origin"->First@Replace["Origin", ds]]];
		If[MemberQ[k,"Y_Bin"], AppendTo[l, "Y_Bin"->First@First@Replace["Y_Bin", ds]]];
		If[MemberQ[k, "Z_Bin"], AppendTo[l, "Z_Bin"->First@First@Replace["Z_Bin", ds]]];
		If[MemberQ[k, "Bands"], AppendTo[l, "Bands"->First@First@Replace["Bands", ds]]];
		If[MemberQ[k,"Format"], AppendTo[l, "Format"->FromCharacterCode[Replace["Format", ds]]]];
		If[MemberQ[k,"Date"], AppendTo[l, "Date"->FromCharacterCode[Replace["Date", ds]]]];
		If[MemberQ[k, "CenterOfRotation"], 
			AppendTo[l, "CenterOfRotation"->First@First@Replace["CenterOfRotation", ds]]];
		If[MemberQ[k,"CentralSlice"], 
			AppendTo[l, "CentralSlice"->First@First@Replace["CentralSlice", ds]]];
		If[MemberQ[k, "RFan_Y"], AppendTo[l, "RFan_Y"->First@First@Replace["RFan_Y", ds]]];
		If[MemberQ[k,"RFan_Z"], AppendTo[l, "RFan_Z"->First@First@Replace["RFan_Z", ds]]];
		If[MemberQ[k,"AngleIncrement"], 
			AppendTo[l, "AngleIncrement"->First@First@Replace["AngleIncrement", ds]]];
		If[MemberQ[k, "ReverseOrder"], 
			AppendTo[l, "ReverseOrder"->FromCharacterCode[Replace["ReverseOrder", ds]]]];
		If[MemberQ[k, "Max"], AppendTo[l, "Max"->First@First@Replace["Max", ds]]];
		If[MemberQ[k,"Spacing"], AppendTo[l, "Spacing"->First@Replace["Spacing", ds]]];
		If[MemberQ[k, "ElementSize"], AppendTo[l, "ElementSize"->First@First@Replace["ElementSize", ds]]];
		If[MemberQ[k, "Water"], AppendTo[l, "Water"->First@First@Replace["Water", ds]]];
		If[MemberQ[k,"Air"], AppendTo[l, "Air"->First@First@Replace["Air", ds]]];
		If[MemberQ[k,"BoneHU"], AppendTo[l, "BoneHU"->First@First@Replace["BoneHU", ds]]];
		If[MemberQ[k, "CmdLine"], AppendTo[l, "CmdLine"->FromCharacterCode[Replace["CmdLine", ds]]]];
		l
	];


Image3DInteractiveSlicer[image3d_Image3D, OptionsPattern[]]:= 
	Switch[ 
		OptionValue[InteractiveViewType], 
		StackView, 
			If[ImageQ[OptionValue[RegionOfInterest]], 
				interactive3DStackWithOverlay[image3d, 
					OptionValue[RegionOfInterest], 
					OptionValue[OverlayColor], 
					OptionValue[OverlayOpacity]],
				interactive3DStack[image3d] 
			], 
		MultiplanarView, 
			If[ImageQ[OptionValue[RegionOfInterest]], 
				interactive3DSlicerWithOverlay[image3d, 
					OptionValue[RegionOfInterest], 
					OptionValue[OverlayColor],
					OptionValue[OverlayOpacity]], 
				interactive3DSlicer[image3d]
			] 
	];


Image3DComparison[img1_Image3D, img2_Image3D, opts : OptionsPattern[{Image3DComparison,Image3D}]]:= 
	DynamicModule[{vv = OptionValue[Graphics3D, ViewPoint], vp = OptionValue[Graphics3D, ViewVertical]},
		Grid[
			{{Image3D[img1, opts, Sequence @@ Options[Image3DComparison], 
				ViewPoint->Dynamic[vv], ViewVertical->Dynamic[vp]],
			Image3D[img2, opts, Sequence @@ Options[Image3DComparison], 
				ViewPoint->Dynamic[vv], ViewVertical->Dynamic[vp]]}}]];


Image3DInteractiveContourPlot[image3d_Image3D, opts : OptionsPattern[{Image3DInteractiveContourPlot, ListContourPlot}]]:=
	With[
		{size = ImageDimensions[image3d],
			contours = Map[
				ListContourPlot[#, opts, 
					Sequence@@FilterRules[Options[Image3DInteractiveContourPlot], Options[ListContourPlot]]
				] &, 
				Map[ImageData[#, DataReversed->True] &, Image3DSlices[ImageAdjust[image3d], All, 1]]
			]
		}, 
		Manipulate[Show[contours[[s]]], {{s, 1, "Slice"}, 1, size[[3]], 1, Slider, Appearance->"Labeled"}]
	]


interactive3DStack[image3d_Image3D]:= 
	Manipulate[ 
		Image3DSlices[ImageAdjust[image3d]][[s]], 
		{{s, 1, "Slice"}, 1, ImageDimensions[image3d][[3]], 1, Slider, Appearance->"Labeled"}];


interactive3DStackWithOverlay[image3d_Image3D, mask_Image3D, overlayColor_, overlayOpacity_]:= 
	Manipulate[ 
		MapThread[
			ImageCompose[ImageAdjust[#], 
				{ImageMultiply[#2, overlayColor], overlayOpacity}
			] &, 
			{Image3DSlices[ImageAdjust[image3d]], Image3DSlices[mask]} 
		][[s]], 
		{{s, 1, "Slice"}, 1, ImageDimensions[image3d][[3]], 1, Slider, Appearance->"Labeled"}];


interactive3DSlicer[image3d_Image3D]:= 
	With[{i = ImageAdjust[image3d], size= ImageDimensions[image3d]}, 
		DynamicModule[{d, e, f}, 
			d = Image3DSlices[i, All, 1];
			e = Image3DSlices[i, All, 2];
			f = Image3DSlices[i, All, 3];
			Manipulate[Grid[{ 
				{Show[ 
					d[[s]], 
					Graphics[{Red, Line[{{c, 0}, {c, size[[2]]}}], 
						Line[{{0, size[[2]]-r+1}, {size[[1]], size[[2]]-r+1}}] 
					}]]}, 
				{Show[ 
					e[[r]], 
					Graphics[{Green, Line[{{c,0},{c, size[[3]]}}], 
						Line[{{0, size[[3]]-s+1},{size[[1]],size[[3]]-s+1}}]}] 
					], 
					Show[ 
						f[[c]],
						Graphics[{Blue, Line[{{r, 0},{r, size[[3]]}}],
							Line[{{0,size[[3]]-s+1},{size[[2]],size[[3]]-s+1}}]}]
					]}
				}],
			{{s, 1, "Slice"}, 1, size[[3]], 1, Slider, Appearance->"Labeled"},
			{{r, 1, "Row"}, 1, size[[2]], 1, Slider, Appearance->"Labeled"},
			{{c, 1, "Column"}, 1, size[[1]], 1, Slider, Appearance->"Labeled"}]]];


interactive3DSlicerWithOverlay[image3d_Image3D, mask_Image3D, overlayColor_, overlayOpacity_]:= 
	With[{i = ImageAdjust[image3d], size= ImageDimensions[image3d]},
		DynamicModule[{d, e, f, dOverlay, eOverlay, fOverlay, dComposed, eComposed, fComposed},
			d = Image3DSlices[i, All, 1];
			dOverlay = Image3DSlices[mask, All, 1];
			e = Image3DSlices[i, All, 2];
			eOverlay = Image3DSlices[mask, All, 2];
			f = Image3DSlices[i, All, 3];
			fOverlay = Image3DSlices[mask, All, 3];
			dComposed = MapThread[ImageCompose[ImageAdjust[#], 
				{ImageMultiply[#2, overlayColor], overlayOpacity}] &, {d, dOverlay}];
			eComposed = MapThread[ImageCompose[ImageAdjust[#], 
				{ImageMultiply[#2, overlayColor], overlayOpacity}] &, {e, eOverlay}];
			fComposed = MapThread[ImageCompose[ImageAdjust[#], 
				{ImageMultiply[#2, overlayColor], overlayOpacity}] &, {f, fOverlay}];
			Manipulate[Grid[{
				{Show[
					dComposed[[s]],
					Graphics[{Red, Line[{{c, 0}, {c, size[[2]]}}],
						Line[{{0, size[[2]]-r+1}, {size[[1]], size[[2]]-r+1}}]
					}]]},
				{Show[
					eComposed[[r]],
					Graphics[{Green, Line[{{c,0},{c, size[[3]]}}],
						Line[{{0, size[[3]]-s+1},{size[[1]],size[[3]]-s+1}}]}]
					],
					Show[
						fComposed[[c]],
						Graphics[{Blue, Line[{{r, 0},{r, size[[3]]}}],
							Line[{{0,size[[3]]-s+1},{size[[2]],size[[3]]-s+1}}]}]
						]}
					}],
				{{s, 1, "Slice"}, 1, size[[3]], 1, Slider, Appearance->"Labeled"},
				{{r, 1, "Row"}, 1, size[[2]], 1, Slider, Appearance->"Labeled"},
				{{c, 1, "Column"}, 1, size[[1]], 1, Slider, Appearance->"Labeled"}]]];

End[];

Protect[ImportMicroCtImage3D, ImportMicroCtImage3DFromDicom, Image3DInteractiveSlicer, RegionOfInterest, InteractiveViewType, OverlayColor, 
OverlayOpacity, StackView, MultiplanarView, SegmentationVolume, VoxelDimensions, AirHounsfieldUnits, 
RegionBlank, BlankSelection, Inside, Outside, BlankValue, GetImageMetaInformation, Image3DComparison,
FindThresholdLocalMinimumMethod, FindThresholdPerpendicularDistanceMethod, 
FindThresholdSecondDerivativeMaximumMethod, FindThresholdFromHistogram, PlotSmoothAndRoughHistograms, 
Image3DInteractiveContourPlot, MaskUnion, MaskIntersection];

EndPackage[];
